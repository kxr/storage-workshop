#!/bin/bash

DESTFILE="/usr/share/netdata/web/index.html"
DEFFILE="/usr/share/netdata/web/default.html"

if [ ! -f "/usr/share/netdata/web/default.html" ]
then
	mv $DESTFILE /usr/share/netdata/web/default.html
fi

echo '<!DOCTYPE html>
<!-- SPDX-License-Identifier: GPL-3.0+ -->
<html lang="en">
<head>
   	<title>Storage Workshop DiskIO Dasboard</title>
	<script type="text/javascript" src="dashboard.js"></script>
	<script type="text/javascript"> NETDATA.options.current.stop_updates_when_focus_is_lost = false; </script>
	<style>
		html, body {margin: 0;} 
		div {border-style: none; margin: 0; padding: 0;}
	</style>
</head>
<body>' > $DESTFILE


for mpath in $(multipathd show paths format "%m" | sed 1d | sort | uniq)
do
	echo "<div style='float: left; width: 24vw; height: 100vh; padding-right: 1%; padding-left: 1%;'>" >> $DESTFILE
	for mdev in $(multipathd show paths format "%m %d" | grep $mpath | awk '{print $2}')
	do

		echo "
		<div style='width: 100%; height: 23%; text-align: center;'>
			<div style='width: 100%; height: 25%; text-align: center;'>
				<div> <h4>$mpath / $mdev</h3> </div>
				<div    data-netdata='disk.$mdev'
				        data-chart-library='gauge'
					data-common-max='$mpath'
					data-title=''
					data-width='50%'
				        data-after='-300'
				        data-points='900'
				        data-colors='#FF5555'>
				</div>
			</div>
			<div style='width: 100%; height: 75%; text-align: center; padding-bottom: 5%;'>
				<div    data-netdata='disk.$mdev'
				        data-chart-library='dygraph'
				        data-dygraph-theme='sparkline'
					data-common-max='$mpath'
					data-height='100%'
					data-width='100%'
				        data-after='-300'
				        data-points='900'
				        data-colors='#FF5555'>
				</div>
			</div>
		</div>
		" >> $DESTFILE
	done

	echo "</div>" >> $DESTFILE
done

echo "
</body>
</html>
" >> $DESTFILE
chown root:netdata $DESTFILE
